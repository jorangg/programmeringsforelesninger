# Renters rente beregnes på følgene måte:
# K = K0 * (1 + R)**n

# Variabler brukeren kan fylle inn
K0 = 150_000
R = 0.05
n = 13

# Beregning av K
K = K0 * (1 + R)**n

# Printe resultat
print(K)
