### Krav-liste
# 1 Må være Mac
# 2 Må har mer enn 1000 GB lagring
# 3 Skjermstørrelse > 16" eller må prisen være mindre enn 15000
# 4 Batteritid > 10 timer eller ladeeffekt > 90W
# 5 Må ikke være brukt

# Fylle inn krav. Dette kan hentes fra input, fil, nettside, database, etc.
er_mac = True
er_brukt = False
lagring = 2000
skjermstørrelse = 15
pris = 14_000
batteritid = 8
ladeeffekt = 140

# Sjekke ett og ett krav
krav1 = (er_mac)
krav2 = (lagring > 1000)
krav3 = (skjermstørrelse > 16) or (pris < 15_000)
krav4 = (batteritid > 10) or (ladeeffekt > 90)
krav5 = not (er_brukt)

# Sjekke om alle krav er opprettholdt, og skrive ut
godkjent = krav1 and krav2 and krav3 and krav4 and krav5
print(godkjent)
