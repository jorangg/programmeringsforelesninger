## Oppdatert/lagt til for Gjøvik-forelesningen
# Her skriver vi koden fra forrige eksempel noe mer kompakt og teknisk.

def er_det_skuddår(årstall):
    return årstall%4 == 0


def dager_i_feb(årstall):
    if er_det_skuddår(årstall):
        antall_dager_i_februar = 29
    else:
        antall_dager_i_februar = 28
    return antall_dager_i_februar


# Her bruker vi funksjoen(e) vi har laget.
årstall = 2024
antall_dager_i_februar = dager_i_feb(årstall)
print(f'Det er {antall_dager_i_februar} dager i februar i år.')


# Enda mer kompakt variant, som vi ikke er pensum dette semesteret.
# <verdi hvis True> if <påstand> else <verdi hvis False>
# Det er greit å ha sette dette, men det er ikke noe du egentlig trenger.
dager_i_februar = 29 if (årstall%4 == 0) else 28
