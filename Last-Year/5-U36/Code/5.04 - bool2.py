# Bruker fyller inn verdier
lagring = 2000  # GB
pris = 18_000

# Sjekke begge delkravene
nok_lagring = (lagring > 1000)
billig_nok = (pris < 13_000)

# Full kravsjekk og utskrift
akseptabel_laptop = (nok_lagring or billig_nok)
print(akseptabel_laptop)
