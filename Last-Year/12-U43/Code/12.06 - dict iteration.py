# Dict kopiert fra forrige fil.
bruker = {
    'navn': 'Obama',
    'land': 'USA',
    'alder': 61
}

# Det er flere måter å iterere over en dict.

# Her iterer vi over hver key, og finner hver verdi som tilhører.
for key in bruker:
    print(key, bruker[key])

# Her itererer vi over verdiene, men ikke keys, med values-funksjonen.
for value in bruker.values():
    print(value)

# Her itererer vi over både keys og values, med items-funksjonen.
for key, value in bruker.items():
    print(key, value)
