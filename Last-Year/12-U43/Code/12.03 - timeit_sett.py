# Koden er ikke pensum, men poenget er å vise forskjellen på list og set.
# Her bruker jeg modulen timeit til å se hvor lang tid koden tar
# med list vs set. Outputet viser at set er LANGT raskere.
import timeit


# list
print(timeit.timeit(
    setup = 'liste = list(range(10_000))',
    stmt = '9001 in liste',
    number = 10_000
))

# set
print(timeit.timeit(
    setup = 'liste = set(list(range(10_000)))',
    stmt = '9001 in liste',
    number = 10_000
))
