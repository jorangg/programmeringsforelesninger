# Med tall kan vi bare kopiere x inn i y sånn.
x = 3
y = x

# Med bl.a. lister og dicts kan vi ikke det.
# Her er b bare en link/peker til a, og listen er det samme.
# Oppdaterer vi a, oppdaterer vi nødvendigvis b også.
a = [1, 2, 3]
b = a

# For å kopiere kan vi bruke copy-modulen. Den er innebygget,
# og trenger derfor ikke å installeres.
import copy

# copy.copy kopierer denne listen (men ikke underlister).
b = copy.copy(a)

# Hvis en skal kopiere f.eks. en liste av lister (tabell),
# kan en bruke copy.deepcopy. Den går dypt i listestrukturen
# eller dict-strukturen og kopierer alt.
tabell = [
    [1, 2],
    [3, 4]
]

c = copy.deepcopy(tabell)
