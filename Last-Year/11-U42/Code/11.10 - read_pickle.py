# Her leser vi inn tabellen vi lagret i forrige program.

import pickle


###################################
## Kopiert fra tidligere oppgave ##
###################################

def print_table(table):
    for row in table:
        for element in row:
            print(f'{element:25}', end=' ')
        print()


###############
## Pickle :D ##
###############

# Lese inn binærdata fra pickle-filen, i read-binary-modus.
with open('data/pickle_testdata', 'rb') as fil:
    bin_data_2 = fil.read()

# Hente tilbake tabellen fra den binære dataen.
tabell = pickle.loads(bin_data_2)

# Printe ut tabellen med print_table-funksjonen vår.
print_table(tabell)
