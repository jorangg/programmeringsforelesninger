# Samme som forrige oppgave, men vi har laget det om til en funksjon.

tekst = '''navn, email, passord
Paul, paul.knutson@ntnu.no, passord123
Joe Biden, potus@whitehouse.gov, trump_is_dumb
Erna, erna@erna.erna, ernaernaerna123'''


def csv_til_tabell(tekst):
    linjer = tekst.split('\n')
    linjer = linjer[1:]

    tabell = []
    for linje in linjer:
        rad = linje.split(', ')
        tabell.append(rad)
    return tabell


def print_table(table):
    for row in table:
        for element in row:
            print(f'{element:25}', end=' ')
        print()


tabell = csv_til_tabell(tekst)
print_table(tabell)
