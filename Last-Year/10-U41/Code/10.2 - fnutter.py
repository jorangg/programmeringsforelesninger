# Vi kan lage strings med både ' og "
tekst = 'hei sann'
tekst = "hei sann"

# Skal vi putte ' i stringen, burde vi bruke "
quote = "It's me, Mario"

# Skal vi bruke " i stringen, burde vi bruker '
quote = 'Og så sa han "så pen du er" og jeg bare lol'

# Skal vi bruke både " og ' i stringen, burde vi enten bruke trippel-fnutt (''' eller """),
# eller escape fnuttene i stringen. Det gjør at Python tolker de som tekst, ikke kode.
# Dette gjør vi ved å putte en \ foran. (Mac: cmd+shit+7, Win: rett til venstre for backspace)
quote = 'She said "it\'s not you, it\'s me". :C'

# Vi kan også escape escape-symbolet, ved å skrive \\. Skal vi skrive "\'" kan
# vi skrive \\\'. \\ blir \ og \' blir '.
print('Hvis du vil escape i en Python-string, kan du skrive dette: "it\\\'s"')

# Vi kan også bruke \n for linje-skift og \t for tab/indentering. Det finnes noen fler slike.
print('Hei!\nMitt navn er\tPaul!')
