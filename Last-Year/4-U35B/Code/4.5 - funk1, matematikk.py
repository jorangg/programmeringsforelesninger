# I matematikken:
# f(x) = x^2
# f(3) = 3^2 = 9

##  I programmering

# Definere funksjonen
def f(x):
    y = x**2
    return y

# Kjøre/kalle funksjonen
y0 = f(0)
y3 = f(3)

# Skrive ut resultatene
print(y0)
print(y3)


## Vi kan også ta inn x-verdien direkte fra brukeren, før vi kjører den i funksjonen, som vist under.
# x = int(input('Skriv inn x '))
# yx = f(x)
# print(yx)
