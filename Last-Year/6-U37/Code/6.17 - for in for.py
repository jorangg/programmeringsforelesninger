## Nested for loops / for-løkke i for-løkke
# Noen ganger kommer vi til å trenge å lage flere for-løkker inn hverandre.
# Dette kan fort bli rotete og komplisert, men kan også være nyttig.
# Programmet under kommer til å skrive ut alle kombinasjoner av x og y.

for x in range(3):
    for y in range(3):
        print(x, y)
