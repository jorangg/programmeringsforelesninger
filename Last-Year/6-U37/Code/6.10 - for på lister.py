# Vi lager en navneliste, og en tom teskter-liste som vi skal fylle.
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']
tekster = []

# for-syntaksen er som følger:
# for <midlertidig variabel> in <itererbart objekt>:

# "navn" er en variabel som får en og en verdi fra navneliste,
# for hver iterasjon.
for navn in navneliste:
    # print(navn, end=' ')

    # Her lager vi teksten med en f-string,
    # og så legger vi den til på slutten av listen "tekster".
    tekst = f'{navn} er kul! :D'
    tekster.append(tekst)

print(tekster)
