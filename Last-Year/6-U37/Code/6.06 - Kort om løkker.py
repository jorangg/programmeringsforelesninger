## Kort om løkkene

# Lage en navneliste vi kan bruke i eksempel 1
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']

# Eksempel 1 - For-loop over itererbart ovbjekt (f.eks. liste)
for navn in navneliste:
    print(f'{navn} er kul! :D')

# Eksempel 2 - For-loop med range
for i in range(10):
    print(i)

# Eksempel 3 - While
x = 0
while x < 10:
    print(x)
    x += 1


## Kort om når vi bruker forskjellige løkketyper:
# Skal du gjenta noe for hvert element i et itererbart objekt? Bruk for (eks 1)
# Om ikke, vet du hvor mange ganger du skal gjenta noe?
#   - Hvis ja: Bruk for og range (eks 2)
#   - Hvis nei: Bruk while (eks 3)
