# range(til)
for i in range(10):
    print(i, end=' ')
print()


# range(fra, til)
for i in range(3, 10):
    print(i, end=' ')
print()


# range(fra, til, steg)
for i in range(10, 20, 2):
    print(i, end=' ')
print()
