# Programmet skal spørre om navn, og si "hei {navn}" igjen og igjen,
# helt til du ikke skriver inn navn, men bare trykker på enter.
# Da avsluttes programmet.

navn = input('Navn: ')
while navn != 'q':
    print('Hei', navn)
    navn = input('Navn: ')
