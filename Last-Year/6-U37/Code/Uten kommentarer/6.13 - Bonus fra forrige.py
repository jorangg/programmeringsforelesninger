import matplotlib.pyplot as plt


def f(x):
    y = x**2
    return y


x_verdier = range(100)
y_verdier = []

for x in x_verdier:
    y = f(x)
    y_verdier.append(y)

print(y_verdier)


plt.plot(x_verdier, y_verdier)
plt.show()
