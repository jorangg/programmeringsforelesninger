# Forrige uke så vi på et eksempel med if-tester som sjekket
# hva slags alkohol, om noe, du kan kjøpe, basert på alderen.
# Her er et eksempel som viser en alternativ måte å gjøre det på.
# Vi putter if-er inni if-er (nesting). Det funker like bra,
# men vi ser ofte positivt på å holde strukturene "flate".
# I.e. ikke ha alt for mye nesting. Det kan fort bli rotete.

alder = 23


if alder >= 18:
    if alder >= 20:
        print('Du kan kjøpe øl og sprit')
    else:
        print('Du kan kjøpe øl')
else:
    print('Du kan ikke kjøpe noe alkohol.')
