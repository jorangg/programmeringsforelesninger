# g(x, t) = t^3 - 4xt + x^2


def g(x, t):
    return t**3 - 4*x*t + x**2


print(g(3, -2))
