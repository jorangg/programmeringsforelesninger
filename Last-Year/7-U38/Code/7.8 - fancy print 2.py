# Hvis vi har lyst til å endre hvilke symbol vi lager veggen rundt teksten med,
# kan vi endre det manuelt, eller lage en variabel (utkommentert), eller
# hente det inn med et argument i funksjonen. Her gir vi 'symbol'
# standardverdien '#', sånn at den forblir '#' med mindre funksjonen mottar
# 2 verdier.
def fancy_print(tekst, symbol='#'):
    # symbol = '#'
    antall = len(tekst) + 4
    print(symbol * antall)
    print(symbol, tekst, symbol)
    print(symbol * antall)


# Vi kan gi verdien som posisjonelt argument, eller ved å presisere.
# Skal funksjoen kunne ta i mot _mange_ verdier, er det greit å presisere.
# Det er det vi gjør når vi skriver ting som 'print(tekst, end=' ')'
fancy_print('Hei sann', '!')
fancy_print('Hei sann', symbol='!')
fancy_print('noe tull')
