'''
Tankemåte:
Oppgaven er litt komplisert, så vi forenkler til bilskilt først.
De består av 2 bokstaver, etterfulgt av 5 tall. Lengden er derfor 7.

'AB12345'


if len(string) != 7:
    return False

elif not string[0:2].isalpha():
    return False

elif not string[2:].isnumeric():
    return False

else:
    return True

01234
-----
A1A
A11A
A1AA
A11AA
'''


def check_registration(regnummer):
    if not (3 <= len(regnummer) <= 5):
        return False
    if not regnummer[0].isalpha():
        return False
    if not regnummer[1].isnumeric():
        return False

    if len(regnummer) == 3 and regnummer[2].isalpha():
        return True
    if len(regnummer) == 5 and regnummer[2].isnumeric() and regnummer[3:].isalpha():
        return True
    if len(regnummer) == 4 and regnummer[2].isalnum() and regnummer[3].isalpha():
        return True
    return False


print(check_registration('N64Ø'))
print(check_registration('P557J'))
