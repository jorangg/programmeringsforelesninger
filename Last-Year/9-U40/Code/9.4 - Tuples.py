# Vi kan lage en liste og oppdatere verdiene.
navneliste = ['Paul', 'Erna']
navneliste[0] = 'Obama'
print(navneliste)

# Vi kan lage en tuple, men ikke oppdatere verdiene i tuplen.
# Det inkluderer ting som sortering o.l.
navnetuple = ('Paul', 'Erna')
# navnetuple[0] = 'Obama'
print(navnetuple)

# Vi kan også lage en tuple uten parentes.
navnetuple2 = 'Paul', 'Erna'
print(navnetuple2)
