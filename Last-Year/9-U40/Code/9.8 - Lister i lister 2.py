# Mer fleredimensjonelle lister (eller tabeller, som vi kaller de her).

# Gangetabellen (fra 0 til 4) er en 5x5 tabell. Vi kan ikke lage en tabell i
# pure Python, men vi kan lage en liste med lister. Tenk på det som en liste
# med rader eller kolonner. F.eks. blir da [[1, 2][3, 4]] en 2x2 tabell som
# ser slik ut:
#   1  2
#   3  4
# Gangetabellen vår burde se noe slikt ut:
#   0   0   0   0   0
#   0   1   2   3   4
#   0   2   4   6   8
#   0   3   6   9  12
#   0   4   8  12  16


# Vi skriver hver liste på en linje, sånn at det blir tydeligere og penere.
# Dette er ikke nødvendig, men gjør det enklere for oss å lese.
gangetabell = [
    [0, 0, 0, 0, 0],
    [0, 1, 2, 3, 4],
    [0, 2, 4, 6, 8],
    [0, 3, 6, 9, 12],
    [0, 4, 8, 12, 16] 
]
print(gangetabell)


# Denne funksjonen skriver ut en tabell.
def print_tabell(tabell):
    # Gå gjennom en og en rad
    for rad in tabell:
        # For hver rad, gå gjennom ett og ett tall.
        for tall in rad:
            # Skrive ut tallet, med kolonnebredde 3.
            print(f'{tall:3}', end=' ')
        # Ny tom linje mellom hver rad.
        print()


print_tabell(gangetabell)
