# Vi kan lage tabeller/2D-lister/lister i lister
# Her er første interne liste fornavnene, og andre interne liste etternavnene.
tabell = [['Paul', 'Erna', 'Barrack'], ['Knutson', 'Solberg', 'Obama']]

# Skriv ut tabellen, en og en rad.
for rad in tabell:
    print(rad)
