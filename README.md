# Programmeringsforelesninger
Hei! Her ligger kode, notater, slides/PowerPoint og andre filer fra forelesningene.


## Om mappene
Mappenenavnene står i form `[Forelesningnr.]-U[Ukenr.]`. Så forelesning 5 i uke 35 heter `5-U35`.
I hver ukemappe ligger det en eller flere av følgende:

- (**!**) en Jupyter-fil (.ipynb) med forelesningsnotater. Dette kan brukes med Jupyter Notebook i nettleseren (se guide lenger nede).
- en mappe ("Code") som har koden jeg skrev i forelesningen.
- en PDF med PowerPoint/Menti-presentasjonen, i de tilfellene jeg har brukt det.
- en mappe ("Slides") med filer som har bygget ev. PDF/Powerpoint.

Og kanskje noe mer, som ikke står her.

I tillegg har vi `Annet`-mappen, med andre ting, som _Python cheat-sheet_ og noen ekstraressurser.
Og vi har `Last-Year`-mappen, hvor vi finner forelesningene fra i fjor, som er nokså like det vi kommer til å gå igjennom. Sånn hvis du vil lese videre før vi kommer dit.


## Forelesningsplan
Planen følger den fra 2022 og kan endres noe etterhvert, men sannsynligvis forblir den ca. riktig.

Sannsynlige rekkefølgeendringer:

- Det kan hende at funksjoner flyttes frem et par uker.
- Det kan også hende vi går litt fortere i starten, siden vi har 3t i stedet for 2t de første to ukene.
- Vi flytter kanskje også dict et stykke frem.

34. Fag- og programmingsintroduksjon
35. Variabler, datatyper, bruk av funksjoner
36. Betingelser og logiske uttrykk
37. Løkker (for og while)
38. Funksjoner
39. Mer om funksjoner, moduler
40. Mer om iterables (lister o.l.)
41. Mer om strings
42. Filer og exceptions
43. Dict og set
44. Repetisjon (1) + ekstramoduler
45. Repetisjon (2)
46. Repetisjon (3)
47. Oppsummering/spørretime


## Mer detaljert forelesningsplan
Igjen, basert på 2022, og kan dermed endres etterhvert.

Under står det hvilke uke vi skal (etter planen) gå igjennom hvilke temaer, og hvilke kapitler i de forskjellige bøkene som er relevant her. Se nedover i dette dokumenter for mer om bøkene (og hva forkortelsene står for).

|Uke|Innhold|Mer om innholdet|SowP|AthswP|PfR|
|---|---|---|--|--|--|
|34|Fag- og programmeringsintroduksjon|* Generell introduksjon til faget <br>* Installasjon av Thonny <br>* Bruk av CLI og editor|1, 2|1||
|35|Variabler, datatyper og bruk av funksjoner|* Variabler <br> * Datatyper og konvertering <br> * Bruk av innebygde funksjoner <br> * Introduksjon til funksjoner|2, 5|1||
|36|Betingelser og logiske uttrykk|* bool <br> * if, elif og else|3|2||
|37|Løkker|* while <br> * for (range og lister) <br> * Lister|4|2, 4||
|38|Funksjoner|* Definisjon/konstruksjon av funksjoner <br> * Input-parametere / argumenter <br> * Returnering av tuples <br> * Namespace|5|3||
|39|Moduler (og mer funksjoner)|* Innebyggede <br> * Installasjon av eksterne moduler i Thonny <br> * Kort om pip|5|||
|40|Mer om iterables|* Lister, tuples og strings <br> * Iterering over itererbare objekter (for) <br> * Mer om indeks <br> * Innbyggede funksjoner (sort, reverse) og len|7|4||
|41|Mer om strings|* Innebyggede funksjoner <br> * Iterering (?)|8|6||
|42|Filer og exceptions|* Metoder å åpne filer <br> * Lese-/skrivemodus <br> * Generell except <br> * Spesifikke except|6|9||
|43|Dict og set|* Intro til begge <br> * Dict-funksjoner og indeksering <br> * Sett i matematikken og i kode|9|5||
|44|Repetisjon (1) + ekstramoduler|* Repetisjon <br> * NumPy <br> * Matplotlib|5|||
|45|Repetisjon (2)|* Repetisjon||||
|46|Repetisjon (3)|* Repetisjon||||
|47|Oppsummerings-/spørretime|* Spørsmål og sånt||||


## Pensumbok
Nei.

Vi har ingen obligatoriske pensumbøker i dette faget. Programmeringsforelesninger, øvingsforelesninger og teoriforelesninger, pluss online ressurser (mer om de senere), skal holde. Men, hvis du alikevel vil ha bøker, har vi et par forslag:

- Starting out with Python (SowP).
- Automate the boring stuff with Python (AthswP).
- Python for Realfag (PfR).

Vi brukte SowP som pensumbok for noen år siden, før vi droppet bok. (Jeg har 5th edition.)
Jeg synes AthswP er en kul bok, fordi den går inn på kule automatiseringsprosjekter. (Jeg har 2nd edition.)
PfR er mer fokusert på matematisk bruk av Python. Den virker også som en god bok for de som liker realfag, men kanskje mindre god hvis du hater matte. (Jeg har 3rd edition.)

Nettressurser jeg ville foreslå å bruke:

- [TDT4110-kompendium](https://www.wikipendium.no/TDT4110_Informasjonsteknologi_grunnkurs/nb/#python): En kort forklaring av det meste relevante i faget IT Grunnkurs (TDT4110). Dette er altså et av parallelfagene våre. Det har ikke helt likt teoripensum, men mye likt programmeringspensum.

- [W3schools](https://www.w3schools.com/python/default.asp): Har mange korte og enkle tutorials på det meste av Python, og en del andre programmeringsspråk.


## Laste ned filer fra GitLab
Tenk på GitLab som en komplisert variant av Google Drive/OneDrive/iCloud/Dropbox, hvor jeg legger filene fra forelesninger, og dere kan laste de ned fra.

For å laste ned, åpne linken til GitLab, trykk på pil-ned-ikonet (ved siden av `Clone`-knappen), og velg `zip`. Da får dere lastet ned hele prosjektet med alle filene.

Noen av filene er Jupyter Notebooks (file extension `.ipynb`). Det finnes programmer som kan åpne de offline (VS Code, Jupyter Notebook/Lab, Anaconda, etc.), men det er ofte ikke verdt å sette opp. Bare les de i GitLab i stedet.


## Åpne Jypyter-filer i GitLab/nettleseren
- Bla gjennom Git-repoet og åpne en fil som slutter på ".ipynb".
- Over filen er det en blå knapp hvor det står "Open in Web IDE", "Edit" eller "Open in Gitpod".
- Åpne undermenyen på denne knappen, og velg "Open in Gitpod.
- Trykk på "hoveddelen" av knappen.
- Da åpner et Gitpod dashboard seg, hvor du må trykke på den grå knappen.
- Så åpner VS Code seg i nettleseren. Du har filstrukturen til repoet på venstre side.

Nå skal du kunne kjøre filer eller celler i filene.
(Det kan hende du må lukke og åpne filen på nytt, hvis den ser teit ut.)
