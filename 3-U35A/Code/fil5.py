# Denne funksjonen kopierte vi fra nettet. Den sjekker om en verdi er et tall.
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


# Kjøre funksjonen flere ganger (med skriver bare ut siste resultat).
result = is_number(3)  # True
result = is_number(3.14)  # True
result = is_number("3")  # True
result = is_number("3.14")  # True
result = is_number("3a")  # False
print(result)


# Forklaring av tingene i funksjonen, som er pensum om mange uker.
# try og except hører sammen. Koden i try-blokken vil kjøres. Hvis det krasjer
# vil koden i except-blokken kjøres. Hvis float-konverteringen ikke funker,
# returner vi False. Hvis den funker, returner vi True.
