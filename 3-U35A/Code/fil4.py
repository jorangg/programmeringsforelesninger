# Funksjoner i Python

# Typisk mattefunksjon
# f(x) = 3x^2


# Definere (men ikke kjøre) funksjonen. Vi lagde z og y.
def f(x):
    y = 3*x**2
    return y


# Kjøre ("kalle"/"call") funksjonen
a = f(3)
print(a)


# NB! Vi kunne returnert utregnet verdi med en gang, i stedet for å lage y.
def f(x):
    return 3*x**2
