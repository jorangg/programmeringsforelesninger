# Input-funksjonen.

# Setup
x = input("Skriv inn x: ")
y = input("Skriv inn y: ")

# Konvertering
x = int(x)
y = int(y)

# Utregning
z = y - x

# Skrive ut resultat
print(z)
