---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

<!-- Visste du at det er flere linjer med kode i en moderne bils software enn i koden som styrte Apollo 11 til månen?
Eller at det er over 2 mrd. linjer med kode i Google sine systemer?
-->
---

![bg 86%](Images/Babbages-Analytical-Engine.jpg)

<!-- På 40-tallet (1840-tallet) designet Charles Babbage hans "Analytic Engine". Den skulle kunne automatisk gjøre matematikk. Basically kalkulator-greier. -->
---

![bg 50%](Images/Ada-Lovelace.jpg)

<!-- Ada Lovelace skrev algoritmer for denne datamaskinen. Hun er kjent som verdens første programmerer.
Hun har også et programmeringsspråk oppkalt etter seg, og mm. -->
---

![bg 66%](Images/Enigma-machine.jpg)

<!-- På 1940-tallet var Tyskland litt ukoselige, og for å holde kommunikasjon hemmelig, algde de Enigma-maskinen.
Den kunne kryptere, eller gjemme, hemmelig informasjon, sånn at tyskerne kunne snakke med hverandre uten at de allierte kunne lese meldingene.
Noen polske matematikere klarte å knekke koden, men det tok veldig lang tid å gjøre dette. -->
---

![bg 53%](Images/Alan-Turing.jpg)

<!-- Alan Turing, sikkert mest kjent fra filmen "The Imitation Game", var med på å lage en datamaskin som kunne knekke koden på 20 minutter, og gi de allierte en stor fordel i krigen.
Og til slutt... -->
---

![bg %](Images/Saturn-V.jpg)
![bg 90%](Images/Apollo-Guidance-Computer.jpg)

<!-- Når NASA skulle sende mennesker til månen, måtte raketten ha et styringssystem som kunne gjøre beregning på orbital mechanics og sånne ting. For å programmere algoritmene deres, trengte de folk som kunne programmere. -->
---

![bg 57%](Images/Margaret-Hamilton.jpg)

<!-- Og en av de som hadde en stor rolle i dette, var Margaret Hamilton. Hun er bl.a. kjent som personen som kom opp med begrepet "software engineer".

Her avbildet ved siden av koden deres.

Fun fact: Dagens kalkulatorer er sterkere enn styringssystemene til Apollo 11. -->
---

## Programmering

Så hva er programmering?

---

*"Programmering består av å designe, skrive, teste, feilsøke og vedlikeholde kildekoden til et program som skal tolkes av en datamaskin."* -Wikipedia

---

## Programmeringsspråk

Og hva er et programmeringsspråk?

---

*"Et programmeringsspråk er en formell måte å uttrykke instruksjoner til en datamaskin på."* -ChatGPT

<!-- For å kunne "snakke" med datamaskiner, trenger vi et språk de skjønner.
Det oversettes så til noe datamaskinen forstår.

Språk skriver ting på forskjellige måter, men logikken er nokså lik.
Chat: Hvilke språk har dere hørt om?
-->
---

## Programmeringsspråk

| Språk | Årstall |
|---|---|
| SQL \* | 1978 |
| C++ | 1980 |
| Matlab | 1984 |
| Python | 1990 |
| Java | 1995 |
| JavaScript | 1995 |
| Rust | 2015 |

<!-- Mange programmeringspråk er gamle. Men logikken er mye av den samme. Dermed blir det nok ikke utdatert med det første. -->

---

## Vårt første program
"Hello, World!"-programmet ([eng](https://en.wikipedia.org//wiki/%22Hello,_World!%22_program), [nor](https://no.wikipedia.org/wiki/Hello,_world))

---

# Praktisk info

<!-- Yay mer praktisk info -->
---

## Om meg
Paul Knutson, 30 år

Universitetslektor
NTNU Gjøvik, A206

paul.knutson@ntnu.no

<!-- Men sjekk Blackboard ang. hvor å sende mail -->
---

## Referansegruppe
- 1 fra Gjøvik
- 1 fra Trondheim
- 1 nettstudent

Send meg mail hvis du er interessert

<!-- 2. året med faget, så stor påvirkning -->
---

## Forelesninger

| Hva | Dag | Tidspunkt | Ansvarlig |
|---|---|---|---|
| Teorif. | Man | 08:15 - 09:00 | Özlem |
| Prog.f. | Ons | 14:15 - 16:00 | Paul |
| Øvingsf. | Tor | 13:15 - 15:00 |Vitass |

Prog. streames til HH.

<!-- Fordel å se på stream i klasserom,
fordi ekstra skjerm -->
---

## Øvinger

Det er planlagt én øving per uke, med unntak av den første og de siste i semesteret. Mer om øvinger finner du på Blackboard.

---

## Nettsystemer

- Blackboard: Info
- Piazza: Q&A
- GitLab: Kode + forelesninger
- Jupyter Hub: Øvinger

<!--  -->
---

## Programmeringssystemer

- Språk: Python
- Kodeprogram: Thonny

<!--  -->
---

## Pensumbok

Nei.

Men det finnes bøker, for de som vil ha. Se Blackboard, GitLab og Akademika.

<!-- Og mange nettressurser -->
---

## Oppstartsinfo
Første uke:
- Ingen øving eller øvingsforelesning
- Fysisk forelesning på HH/Trondheim
- Uformell øving: Neste slide

Første to uker:
- Programmering i stedet for teori.

---

## Uformelle øving/lekse
Før onsdag:
- Sett deg litt inn i BB, GitLab og Piazza.
- Installer Thonny, og sjekk at det funker.
- Se gjerne litt på dokumentene som ligger på GitLab.

---

## Guide
- GitLab
- Piazza
- Installere Thonny
- Python basics

<!-- Paul viser -->
